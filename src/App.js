import logo from './logo.svg';
import './App.css';
import ThuKinh from './ThuKinh/ThuKinh';

function App() {
  return (
    <div>
      <ThuKinh />
    </div>
  );
}

export default App;
