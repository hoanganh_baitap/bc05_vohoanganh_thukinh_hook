import React, { useState } from "react";
import Kinh from "./Kinh";
import data from "./data/dataGlasses.json";
import Model from "./Model";

export default function ThuKinh() {
  const [kinh, setKinh] = useState({
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./img/glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    });
    // console.log('kinh: ', kinh);
  // ham thay doi kinh
  let doiKinh = (kinhMoi) => {
    setKinh(kinhMoi)
  }
  return (
    <div className="row container mx-auto mt-5">
      <div className="col-6">
        <Model kinh = {kinh}/>
      </div>
      <div className="col-6">
        <h4>Vui lòng chọn kính</h4>
        <div className="row">
          <Kinh doiKinh={doiKinh} data={data} />
        </div>
      </div>
    </div>
  );
}
