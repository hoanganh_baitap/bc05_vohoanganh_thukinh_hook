import React from "react";

export default function Model({ kinh }) {
  return (

      <div className="card p-5 position-relative" style={{border: "none", fontSize: "20px"}}>
        <img
          className="card-img-top"
          src="./img/glassesImage/model.jpg"
          alt=""
        />
        <div className="position-absolute" style={{top: 180, left: 153}}>
          <img className="" style={{ width: 220 }} src={kinh.url} alt="" />
        </div>
        <div className="card-body bg-dark text-light">
          
          <p className="card-text">{kinh.name}</p>
          <p className="card-text">{`${kinh.price} $`}</p>
          <p className="card-text">{kinh.desc}</p>

        </div>
      </div>

  );
}
