import React from 'react'

export default function Kinh({data, doiKinh}) {
  console.log('data: ', data);
  return data.map((kinh, index)=>{
    return <div key={index} className='col-2 p-5 m-2'>
  
        <img onClick={()=>{doiKinh(kinh)}} style={{width:100, cursor: "pointer"}} src={kinh.url} alt="" />
    </div>
  })
}
